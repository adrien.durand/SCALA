package main

import SquerylEntryPoint._

class Parser(database: Database) {
  val runwayToParse = List("id", "airport_ref", "surface", "le_ident", "length_ft", "lighted", "closed" )
  val airportToParse = List("id", "iso_country", "name", "municipality")
  val countryToParse = List("code", "name")

  def initDb(runway : String, countries : String, airport : String) : Boolean = {
    parse(countries, countryToParse, addToCountry)
    parse(airport, airportToParse, addToAirport)
    parse(runway, runwayToParse, addToRunway)
  }

  case class IndCol(ind : Int, col : String)

  def getIndFromString(names : List[String], row: Array[String]) : List[IndCol] = {
    names.foldLeft(List[IndCol]())((acc, colName) => IndCol(row.indexOf(colName), colName)::acc)
  }

  def mySplit(str: String, cur: StringBuilder = new StringBuilder(), ind: Int = 0, quoting: Boolean = false,
              res: Array[String] = Array()) : Array[String] = {
    if (ind == str.size)
      res :+ cur.toString()
    else {
      (str(ind), quoting) match {
        case ('"', _) if ind == 0 => mySplit(str, cur, ind + 1, !quoting, res)
        case ('"', _) if str(ind - 1) != '\\' => mySplit(str, cur, ind + 1, !quoting, res)
        case ('"', _) => mySplit(str, cur, ind + 1, quoting, res)
        case (',', true) => mySplit(str, cur.append(','), ind + 1, quoting, res)
        case (',', false) => mySplit(str, new StringBuilder(), ind + 1, quoting, res :+ cur.toString())
        case (c, _) => mySplit(str, cur.append(c), ind + 1, quoting, res)
      }
    }
  }

  def parse(file : String, col: List[String], addToDb : (List[IndCol], Array[String]) => Unit) : Boolean =
  {
    val bufferedSource = io.Source.fromFile(file)
    val lines = bufferedSource.getLines
    if (!lines.hasNext)
      false
    else {
      // get index of each column from first line
      val ind = getIndFromString(col, mySplit(lines.next()))
      transaction {
        lines.foreach({ line =>
          val colsInd = mySplit(line)
          addToDb(ind, colsInd)
        })
      }
      bufferedSource.close
      true
    }
  }

  def toInt(str: String) : Int = {
    if (str.equals(""))
      0
    else
      str.toInt
  }

  def toBool(str: String) : Boolean = {
    if (str.equals("1"))
      true
    else
      false
  }

  def getColFromName(list: List[IndCol], name: String) : IndCol = {
    list match {
      case head::Nil => head
      case head::_ if head.col.equals(name) => head
      case _::tail => getColFromName(tail, name)
    }
  }

  def addToAirport(indCols : List[IndCol], cols : Array[String]) : Unit = {
    database.insertAirport(toInt(cols(getColFromName(indCols, "id").ind)),
      cols(getColFromName(indCols, "iso_country").ind),
      cols(getColFromName(indCols, "name").ind),
      cols(getColFromName(indCols, "municipality").ind))
  }

  def addToRunway(indCols : List[IndCol], cols : Array[String]) : Unit = {
    database.insertRunway(toInt(cols(getColFromName(indCols, "id").ind)),
      toInt(cols(getColFromName(indCols, "airport_ref").ind)),
      cols(getColFromName(indCols, "surface").ind),
      cols(getColFromName(indCols, "le_ident").ind),
      toInt(cols(getColFromName(indCols, "length_ft").ind)),
      toBool(cols(getColFromName(indCols, "lighted").ind)),
      toBool(cols(getColFromName(indCols, "closed").ind)))
  }

  def addToCountry(indCols : List[IndCol], cols : Array[String]) : Unit = {
    database.insertCountry(cols(getColFromName(indCols, "code").ind),
      cols(getColFromName(indCols, "name").ind) )
  }
}
