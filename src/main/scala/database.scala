package main

import SquerylEntryPoint._
import org.squeryl._
import org.squeryl.adapters._
import org.squeryl.dsl.Group
import org.squeryl.logging.LocalH2SinkStatisticsListener
import scala.language.postfixOps

class Database {
  def initDatabase(country : String = "src/ressources/countries.csv",
                   airport: String = "src/ressources/airports.csv", runway: String = "src/ressources/runways.csv") = {
    // creation of the session link to the database
    Class.forName("org.h2.Driver")

    val localH2SinkStatisticsListener = LocalH2SinkStatisticsListener.initializeOverwrite("~/db-usage-stats")

    SessionFactory.concreteFactory = Some(() =>
      new Session(java.sql.DriverManager.getConnection("jdbc:h2:~/test", "sa", ""),
        new H2Adapter,
        Some(localH2SinkStatisticsListener)
      )
    )

    // creation of the tables
    transaction {
      Table.drop
      Table.create
    }
    new Parser(this).initDb(runway, country, airport)
  }

  def insertCountry(code: String, name: String): Unit = {
    val country = new Country(code, name)
    Table.countries.insert(country)
  }

  def insertRunway(id: Int, airportRef: Int, surface: String, leIdent: String, length: Int, light: Boolean, closed: Boolean): Unit = {
    val runway = new Runway(id, airportRef, surface, leIdent, length, light, closed)
    Table.runways.insert(runway)
  }

  def insertAirport(id: Int, isoCountry: String, name: String, municipality: String): Unit = {
    val airport = new Airport(id, isoCountry, name, municipality)
    Table.airports.insert(airport)
  }

  def printAirportsAndRunwaysFromCountry(code: String) : Unit =
  {
    val airports = from(Table.airports)(airport => where(airport.isoCountry === code) select(airport))
    if (airports.size == 0)
      println("The country with code " + code + " does not contain any airport.")
    else {
      println("The country with code " + code + " contains the following airports:")
      airports.foreach({
        airport =>
          println("Airport: \"" + airport.name + "\" located in: " + airport.municipality)
          val runways = from(Table.runways)(runway => where(runway.airportRef === airport.id) select (runway))
          runways.foreach(runway => println("\tRunway: " + runway.id + " of length: " + runway.lengthFt + "ft, lighted: " + runway.lighted + ", closed: " + runway.closed))
      })
    }
  }

  def printTopCountryAirport() : Unit =
  {
    println("The 10 countries with the largest number of airports:")
    transaction {
      val countries = join(Table.countries, Table.airports.leftOuter)((country, airport) => groupBy(country.name)
        compute(countDistinct(airport.get.id))
        orderBy(countDistinct(airport.get.id) desc)
        on(country.code === airport.get.isoCountry)
      ).page(0, 10)
      countries.foreach(x => println("\t" + x.key + ": " + x.measures))
    }
  }

  def printTopLeastCountryAirport() : Unit =
  {
    println("The 10 countries with the smallest number of airports:")
    transaction {
      val countries = join(Table.countries, Table.airports.leftOuter)((country, airport) =>
        groupBy(country.name)
          compute(countDistinct(airport.get.id))
          orderBy(countDistinct(airport.get.id), country.name)
          on(country.code === airport.get.isoCountry)
      ).page(0, 10)
      countries.foreach(x => println("\t" + x.key + ": " + x.measures))
    }
  }

  private def printSurfacePerCountry(countriesSurfaces : Iterator[Group[Product2[String, String]]],
                                     country : String = "") : Unit = {
    if (countriesSurfaces.hasNext) {
      val countrySurface = countriesSurfaces.next()
      if (!countrySurface.key._1.equals(country) && !countrySurface.key._2.equals("")) {
        println("Surfaces in " + countrySurface.key._1 + ':')
        println("\t" + countrySurface.key._2)
        printSurfacePerCountry(countriesSurfaces, countrySurface.key._1)
      } else {
        if (!countrySurface.key._2.equals(""))
          println("\t" + countrySurface.key._2)
        printSurfacePerCountry(countriesSurfaces, country)
      }
    }
  }

  def printSurfacePerCountry(): Unit = {
    transaction {
      val countries = join(Table.runways, Table.airports, Table.countries)((runway, airport, country) =>
        groupBy(country.name, runway.surface)
          orderBy(country.name)
          on(runway.airportRef === airport.id, airport.isoCountry === country.code)
      )
      printSurfacePerCountry(countries.iterator)
    }
  }

  def printMostCommonRunLat(): Unit = {
    println("The top 10 most common runway latitude:")
    transaction {
      val latitudes = from(Table.runways)((runway) =>
        groupBy(runway.leIdent)
          compute (countDistinct(runway.id))
          orderBy (countDistinct(runway.id) desc)
      ).page(0, 10)
      latitudes.foreach(x => println("\t" + x.key))
    }
  }

  def printAirportsFromCountry(input: String) : Unit = {
    if (input.size == 2)
      transaction {
        printAirportsAndRunwaysFromCountry(input)
      }
    else {
      try {
        transaction {
          val code = from(Table.countries)(country => where(lower(country.name) like lower(input.toLowerCase + "%"))
            select (country.code)).page(0, 1).single
          printAirportsAndRunwaysFromCountry(code)
        }
      } catch {
        case _: NoSuchElementException => println("Country not found try again.")
      }
    }
  }
}
