PROJECT
=======

This project is a simple terminal that will print information about countries,
airports and runways. Those information are taken from 3 different CSV files.

DESCRIPTION
===========

To run this project, be sure that sbt is installed on your computer.
Then you can run "sbt compile" at the root directory of this project and then
"sbt run [ARGS]". ARGS being the path of the CSV files, the three files should
be given as a parameter. If you do not give all of them then the CSV files will
be those located in "src/ressources/".

The order of the column in the csv files are not important but their name are:
* the airport.csv should at least contain columns: "id", "iso_country", "name",
  "municipality"
* the country.csv should at least contain columns: "code", "name"
* the runway.csv should at least contain columns: "id", "airport_ref",
  "surface", "le_ident"

Once the program is running you can enter "Query" or "Reports":
* Query: You should enter a name or the code of a country and then it will print
  all the airports and the associated runways of the country.
* Reports: will display the following:
  * 10 countries with highest number of airports (with count) and countries 
    with lowest number of airports.
  * Type of runways (as indicated in "surface" column) per country
  * The top 10 most common runway latitude (indicated in "le_ident" column)


TEST
====

To run the test suite, use:
  $ sbt test

To run the test suite on only one class, use:
  $ sbt "testOnly *<TestClassName>"
ex: sbt "testOnly *DatabaseSpec"

For the test we used scalatest.

IMPLEMENTATION
==============

The code is divided in 5 files:
* "main.scala": is the entrance point of the program, it initializes the
  database and launches the terminal.
* "database.scala": contains the methods for initialisation and communication
  with the database. 
* "parser.scala": will parse the csv files to put them in the database
* "term.scala": launches the term, loops and executes the queries entered by the
  user.

In addition to the mandatory part we did the "name matching partial/fuzzy. e.g.,
entering zimb will result in Zimbabwe" and we used a database in h2. To interact
with the database we used the dependency squeryl which is a Scala ORM and DSL
for talking with Databases. We use an H2 database which is embeded
(in-memory database).

The database relation representation could be seen in
"src/main/scala/table.scala" which define three tables:
* COUNTRY: (code PRIMARY KEY, name)
* AIRPORT: (id PRIMARY KEY, isoCountry FOREIGN KEY on COUNTRY.code, municipality)
* RUNWAY: (id PRIMARY KEY, aiportRef FOREIGN KEY on AIRPORT.id, surface, leIdent)
