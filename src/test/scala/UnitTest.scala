package main

import org.scalatest.FlatSpec

abstract class UnitTest(component: String) extends FlatSpec {
  behavior of component
}

object UnitTest {
  val database = new Database()
  database.initDatabase()
}
