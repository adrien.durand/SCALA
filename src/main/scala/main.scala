package main

object Main extends App {
  println("Initialisation ...")
  val database = new Database()
  if (args.size != 3)
    database.initDatabase()
  else
    database.initDatabase(args(0), args(1), args(2))
  println("Initialisation terminée")
  new Term(database).start
}
