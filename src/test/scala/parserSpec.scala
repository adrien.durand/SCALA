package main

import java.io.ByteArrayOutputStream

class ParserSpec extends UnitTest("Parser") {

  var parser = new Parser(UnitTest.database)

  it should "return false when files is empty (parse)" in {
    assert(!parser.parse("src/ressources/empty.csv", List(), null))
  }

  it should "split normal csv line (mySplit)" in {
    assert(parser.mySplit("1, 2, 3") === Array("1", " 2", " 3"))
  }

  it should "handle line without comma (mySplit)" in {
    assert(parser.mySplit("1 3") === Array("1 3"))
  }

  it should "not split when a comma is between quotes (mySplit)" in {
    assert(parser.mySplit("1, \"2, 3\"") === Array("1", " 2, 3"))
  }
  
  it should "not split when all the line is between quotes (mySplit)" in {
    assert(parser.mySplit("\"1, 2, 3\"") === Array("1, 2, 3"))
  }

  it should "skip escaped quote (mySplit)" in {
    assert(parser.mySplit("1 \"3") === Array("1 3"))
  }
}
