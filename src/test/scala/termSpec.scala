package main

import java.io.{ByteArrayOutputStream, StringReader}
import scala.io.Source

class TermSpec extends UnitTest("Term") {

  var term = new Term(UnitTest.database)

  it should "print \"GI\" (=Gibraltar) airports (query)" in {
    val input = new StringReader(s"GI\n")
      val output = new ByteArrayOutputStream
      Console.withIn(input) {
        Console.withOut(output){
          term.query()
        }
      }
    assert(output.toString
        == "Enter the name or the code of a country\nThe country with code GI contains the following airports:\n"+
        "Airport: \"Gibraltar Airport\" located in: Gibraltar\n\tRunway: 236097 of length: 6000ft, lighted: true, closed: false\n")
  }
  
  it should "print \"GIBRALTAR\" airports (query)" in {
    val input = new StringReader(s"GIBRALTAR\n")
      val output = new ByteArrayOutputStream
      Console.withIn(input) {
        Console.withOut(output){
          term.query()
        }
      }
    assert(output.toString
        == "Enter the name or the code of a country\nThe country with code GI contains the following airports:\n"+
        "Airport: \"Gibraltar Airport\" located in: Gibraltar\n\tRunway: 236097 of length: 6000ft, lighted: true, closed: false\n")
  }

  it should "print a Report (report)" in {
    val output = new ByteArrayOutputStream
      Console.withOut(output){
        term.report()
      }
    assert(output.size() > 17500)
  }
}
