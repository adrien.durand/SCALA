package main

import org.squeryl._
import org.squeryl.dsl._

object SquerylEntryPoint extends PrimitiveTypeMode {

  class Country(val code: String,
                val name: String) extends KeyedEntity[String] {
    val id = code
    lazy val airports: OneToMany[Airport] = Table.countryAirports.left(this)
  }

  class Airport(val id: Int,
                val isoCountry: String,
                val name: String,
                val municipality: String) extends KeyedEntity[Int] {
    lazy val country: ManyToOne[Country] = Table.countryAirports.right(this)
    lazy val runways: OneToMany[Runway] = Table.airportRunways.left(this)
  }

  class Runway(val id: Int,
               val airportRef: Int,
               val surface: String,
               val leIdent: String,
               val lengthFt: Int,
               val lighted: Boolean,
               val closed: Boolean) extends KeyedEntity[Int] {
    lazy val airport: ManyToOne[Airport] = Table.airportRunways.right(this)
  }


  object Table extends Schema {
    val countries = table[Country]
    val airports = table[Airport]
    val runways = table[Runway]

    //add those constraints to remove the autoincrement added by KeyedEntity
    on(airports)(airport => declare(
      airport.id is (primaryKey)
    ))

    on(runways)(runway => declare(
      runway.id is (primaryKey)
    ))

    val airportRunways = oneToManyRelation(airports, runways).
      via((airport, runway) => airport.id === runway.airportRef)

    val countryAirports = oneToManyRelation(countries, airports).
      via((country, airport) => country.id === airport.isoCountry)
  }
}
