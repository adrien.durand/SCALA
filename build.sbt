import Dependencies._

mainClass in Compile := Some("main.Main")

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.durand_v.faure_s",
      scalaVersion := "2.12.5",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "Main",
    libraryDependencies += squeryl,
    libraryDependencies += scalatest,
    libraryDependencies += h2
  )
