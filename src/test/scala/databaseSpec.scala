package main

import SquerylEntryPoint._
import java.io.ByteArrayOutputStream

class DatabaseSpec extends UnitTest("Database") {

  it should "print error if country given doesn't exist (printAirportsFromCountry)" in {
    val output = new ByteArrayOutputStream
      Console.withOut(output) {
        UnitTest.database.printAirportsFromCountry("Asgard")
      }
    assert(output.toString == "Country not found try again.\n")
  }

  it should "print a different message if country doesn't have airports(printAirportsAndRunwaysFromCountry)" in {
    val output = new ByteArrayOutputStream
    Console.withOut(output) {
      UnitTest.database.printAirportsFromCountry("PN")
    }
    assert(output.toString == "The country with code PN does not contain any airport.\n")
  }

  it should "print airports corresponding to the given country name (printAirportsFromCountry)" in {
    val output = new ByteArrayOutputStream
      Console.withOut(output) {
        UnitTest.database.printAirportsFromCountry("Vatican")
      }
    assert(output.toString
        == "The country with code VA contains the following airports:\n"
        + "Airport: \"Vatican City Heliport\" located in: \n")
  }

  it should "print airports corresponding to the given country code (printAirportsFromCountry)" in {
    val output = new ByteArrayOutputStream
      Console.withOut(output) {
        UnitTest.database.printAirportsFromCountry("VA")
      }
    assert(output.toString
        == "The country with code VA contains the following airports:\n"
        + "Airport: \"Vatican City Heliport\" located in: \n")
  }

  it should "print most common runways latitude (printMostCommonRunLat)" in {
    val output = new ByteArrayOutputStream
      Console.withOut(output) {
        UnitTest.database.printMostCommonRunLat()
      }
    assert(output.toString
        == "The top 10 most common runway latitude:\n\tH1\n\t18\n\t09\n\t17\n\t16\n\t12\n\t14\n\t08\n\t13\n\t15\n")
  }

  it should "print the 10 countries with the smallest number of airports (printTopLeastCountryAirport)" in {
    val output = new ByteArrayOutputStream
      Console.withOut(output) {
        UnitTest.database.printTopLeastCountryAirport()
      }
    assert(output.toString
        == "The 10 countries with the smallest number of airports:\n\tPitcairn: 0\n"
        + "\tSouth Georgia and the South Sandwich Islands: 0\n\tTokelau: 0\n\tAndorra: 1\n\tAnguilla: 1\n\tAruba: 1\n"
        + "\tBritish Indian Ocean Territory: 1\n\tChristmas Island: 1\n\tCocos (Keeling) Islands: 1\n\tCuraçao: 1\n")
  }

  it should "print the 10 countries with the largest number of airports (printTopLeastCountryAirport)" in {
    val output = new ByteArrayOutputStream
      Console.withOut(output) {
        UnitTest.database.printTopCountryAirport()
      }
    assert(output.toString == "The 10 countries with the largest number of airports:\n\tUnited States: 21501\n"
        + "\tBrazil: 3839\n\tCanada: 2454\n\tAustralia: 1908\n\tRussia: 920\n\tFrance: 789\n\tArgentina: 713\n"
        + "\tGermany: 703\n\tColombia: 700\n\tVenezuela: 592\n")
  }

  it should "insert a country in the database (insertCountry)" in {
    val output = new ByteArrayOutputStream
      transaction {
        UnitTest.database.insertCountry("00", "TESTcountry")
      }
    Console.withOut(output) {
      UnitTest.database.printAirportsFromCountry("00")
    }
    assert(output.toString == "The country with code 00 does not contain any airport.\n")
  }

  it should "insert an airport in the database (insertAirport)" in {
    val output = new ByteArrayOutputStream
      transaction {
        UnitTest.database.insertAirport(99999, "00", "TESTairport", "TESTmunicipality")
      }
    Console.withOut(output) {
      UnitTest.database.printAirportsFromCountry("00")
    }
    assert(output.toString == "The country with code 00 contains the following airports:\nAirport: \"TESTairport\" located in: TESTmunicipality\n")
  }

  it should "insert a runway in the database (insertRunway)" in {
    val output = new ByteArrayOutputStream
      transaction {
        UnitTest.database.insertRunway(90001, 99999, "4242", "SCALArunway", 100, true, false)
      }
    Console.withOut(output) {
      UnitTest.database.printAirportsFromCountry("00")
    }
    assert(output.toString
    == "The country with code 00 contains the following airports:\nAirport: \"TESTairport\" located in: TESTmunicipality\n\tRunway: 90001 of length: 100ft, lighted: true, closed: false\n")
  }

}
