package main

class Term(database: Database) {
  def start() : Unit =
  {
    println("Enter:\n" +
      "\"Query\" to have information on a country\n" +
      "\"Reports\" to have general information\n" +
      "\"Quit\" to exit")
    val input = scala.io.StdIn.readLine();
    if (input.toLowerCase.equals("query"))
      query()
    else if (input.toLowerCase().equals("reports"))
      report()
    else if (input.toLowerCase().equals("quit"))
      System.exit(0)
    start()
  }

  def query() : Unit =
  {
    println("Enter the name or the code of a country")
    val input = scala.io.StdIn.readLine()
    database.printAirportsFromCountry(input)
  }

  def report() : Unit =
  {
    database.printTopCountryAirport()
    println()
    database.printTopLeastCountryAirport()
    println()
    database.printSurfacePerCountry()
    println()
    database.printMostCommonRunLat()
  }
}
