import sbt._

object Dependencies {
  lazy val squeryl = "org.squeryl" %% "squeryl" % "0.9.11"
  lazy val scalatest = "org.scalatest" % "scalatest_2.12" % "3.0.5" % "test"
  val h2 = "com.h2database" % "h2" % "1.2.127"
}
